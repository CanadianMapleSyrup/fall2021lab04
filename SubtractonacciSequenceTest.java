package fall2021lab04;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SubtractonacciSequenceTest {

    @Test
    public void getFirstTermTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(2,4);
        int result = testSeq.getTerm(1);
        assertEquals(2, result);
    }

    @Test
    public void getSecondTermTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(2,4);
        int result = testSeq.getTerm(2);
        assertEquals(4, result);
    }

    @Test
    public void getThirdTermTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(2,4);
        int result = testSeq.getTerm(3);
        assertEquals(-2, result);
    }

    @Test
    public void getFifthTermTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(2,4);
        int result = testSeq.getTerm(5);
        assertEquals(-8, result);
    }

    @Test
    public void ZeroSequenceTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(0,0);
        int result = testSeq.getTerm(9);
        assertEquals(0, result);
    }

    @Test
    public void firstNegativeSequenceTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(-2,4);
        int result = testSeq.getTerm(3);
        assertEquals(-6, result);
    }

    @Test
    public void secondNegativeSequenceTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(2,-4);
        int result = testSeq.getTerm(3);
        assertEquals(6, result);
    }

    @Test
    public void fullNegativeSequenceTest()
    {
        SubtractonacciSequence testSeq = new SubtractonacciSequence(-2,-4);
        int result = testSeq.getTerm(3);
        assertEquals(2, result);
    }

    @Test
    public void doubleGetTest()
    {
        FibonacciSequence fs = new FibonacciSequence(-2, -4);
        int term = fs.getTerm(3);
        term = fs.getTerm(3);
        assertEquals(-6, term);
    }
}
