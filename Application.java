package fall2021lab04;
import java.util.Scanner;
/**
 * Application
 */
public class Application {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a your string of sequences");
        String stringSequences = input.next();
        Sequence[] sequences = parse(stringSequences);
        print(sequences,10);
        input.close();
        
    }

    public static void print(Sequence[] sequences, int n)
    {
        for (Sequence sequence : sequences)
        {
            int tempN = n;
            StringBuilder stringSequence = new StringBuilder();
            for (int i = 0; i < tempN; i++)
            {
                if (tempN-i == 1)
                {
                    stringSequence.insert(0,sequence.getTerm(tempN-i));
                }
                else
                {
                    stringSequence.insert(0,"," + sequence.getTerm(tempN-i));
                }
                
            }
            if (sequence instanceof FibonacciSequence)
            {
                stringSequence.insert(0,"Fib:");
            }
            else if (sequence instanceof SubtractonacciSequence)
            {
                stringSequence.insert(0,"Sub:");
            }
            System.out.println(stringSequence);
        }
    }

    public static Sequence[] parse(String toBeParsed)
    {
        String[] values = toBeParsed.split(";");
        String value;
        Sequence[] sequences = new Sequence[values.length/4];
        int valuePos = 1;
        int sequencePos = 0;
        String type = "";
        int firstNum = 0;
        int secondNum = 0;
        
        for (int position = 0; position < values.length; position ++)
        {
            value = values[position];
            
            if (valuePos == 1)
            {
                type = value;
                valuePos ++;
            }
            else if (valuePos == 2)
            {
                firstNum = Integer.parseInt(value);
                valuePos ++;
            }
            else if (valuePos == 3)
            {
                secondNum = Integer.parseInt(value);
                valuePos ++;
            }
            else
            {
                
                if (type.equals("Fib"))
                {
                    sequences[sequencePos] = new FibonacciSequence(firstNum, secondNum);
                }
                else
                {
                    sequences[sequencePos] = new SubtractonacciSequence(firstNum, secondNum);
                }
                valuePos = 1;
                sequencePos ++;

            }
        }
        return sequences;
    }
    
}