package fall2021lab04;

public class FibonacciSequence extends Sequence{
    
    public FibonacciSequence(int num1, int num2) {
        super(num1, num2);
    }

    public int getTerm(int n)
    {
        int temp = 0;
        int previous = num1;
        int current = num2;

        if (n <= 0)
        {
            throw new IllegalArgumentException("Cannot get 0th or lower term.");
        }
        else if (n == 1)
        {
            return previous;
        }
        else if (n == 2)
        {
            return current;
        }
        else
        {
            for (int x = 3; x <= n; x ++)
            {
                temp = previous + current;
                previous = current;
                current = temp;
            }
        }

        
        return current;
    }
}
