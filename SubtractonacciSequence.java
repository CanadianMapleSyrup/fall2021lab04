package fall2021lab04;

public class SubtractonacciSequence extends Sequence 
{
    
    public SubtractonacciSequence(int num1, int num2) 
    {
        super(num1, num2);
    }

    public int getTerm(int n) 
    {
        int temp = 0;
        int previous = this.num1;
        int current = this.num2;

        if (n <= 0)
        {
            throw new IllegalArgumentException("Cannot get 0th or lower term.");
        }
        else if (n == 1) 
        {
            return this.num1;
        }
        else if (n == 2)
        {
            return this.num2;
        }
        else
        {
            for (int i = 2; i < n; i++)
            {
                temp = previous - current;
                previous = current;
                current = temp;
            }
            return current;
        }
    }
}
