package fall2021lab04;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class FibonacciSequenceTest {
    @Test
    public void getFirstTermTest()
    {
        FibonacciSequence fs = new FibonacciSequence(2, 4);
        int term = fs.getTerm(1);
        assertEquals(2, term);
    }

    @Test
    public void getSecondTermTest()
    {
        FibonacciSequence fs = new FibonacciSequence(2, 4);
        int term = fs.getTerm(2);
        assertEquals(4, term);
    }

    @Test
    public void getThirdTermTest()
    {
        FibonacciSequence fs = new FibonacciSequence(2, 4);
        int term = fs.getTerm(3);
        assertEquals(6, term);
    }


    @Test
    public void getFifthTermTest()
    {
        FibonacciSequence fs = new FibonacciSequence(2, 4);
        int term = fs.getTerm(5);
        assertEquals(16, term);
    }

    @Test
    public void zeroSequenceTest()
    {
        FibonacciSequence fs = new FibonacciSequence(0, 0);
        int term = fs.getTerm(10);
        assertEquals(0, term);
    }

    @Test
    public void firstNegativeTest()
    {
        FibonacciSequence fs = new FibonacciSequence(-2, 4);
        int term = fs.getTerm(3);
        assertEquals(2, term);
    }

    @Test
    public void secondNegativeTest()
    {
        FibonacciSequence fs = new FibonacciSequence(2, -4);
        int term = fs.getTerm(3);
        assertEquals(-2, term);
    }

    @Test
    public void fullNegativeTest()
    {
        FibonacciSequence fs = new FibonacciSequence(-2, -4);
        int term = fs.getTerm(3);
        assertEquals(-6, term);
    }

    @Test
    public void doubleGetTest()
    {
        FibonacciSequence fs = new FibonacciSequence(-2, -4);
        int term = fs.getTerm(3);
        term = fs.getTerm(3);
        assertEquals(-6, term);
    }
}
